import { useEffect, useState } from "react";

const UserForm = () => {
    const [firstname, setFirstname] = useState(localStorage.getItem("firstName"));
    const [lastname, setLastname] = useState(localStorage.getItem("lastName"));

    const onChangeFirstNameHandler = (event) => {
        console.log("firstname is changing...");
        localStorage.getItem("firstName", event.target.value); // Lưu key: firstname vào localStorage
        setFirstname(event.target.value);
    }
    const onChangeLastNameHandler = (event) => {
        console.log("lastname is changing...");
        localStorage.getItem("lastname", event.target.value);
        setLastname(event.target.value);
    }
    useEffect(() => {
        console.log("ComponentDidUpdate");
        localStorage.setItem("firstName", firstname);
        localStorage.setItem("lastName", lastname);
    })
    return (
        <div className="div-input">
            <input placeholder="Firstname" onChange={onChangeFirstNameHandler} value={firstname}></input>
            <br />
            <input placeholder="Lastname" onChange={onChangeLastNameHandler} value={lastname}></input>
            <p>{firstname} {lastname}</p>
            <p>Devcamp User</p>
        </div>
    )
}
export default UserForm;
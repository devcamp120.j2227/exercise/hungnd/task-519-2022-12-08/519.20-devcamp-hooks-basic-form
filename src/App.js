import UserForm from "./Components/UserForm";

import './App.css';

function App() {
  return (
    <div className="container">
        <UserForm/>
    </div>
  );
}

export default App;
